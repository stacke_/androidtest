package br.com.digio.androidtest.data

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.platform.app.InstrumentationRegistry
import br.com.digio.androidtest.data.local.AppDatabase
import br.com.digio.androidtest.data.local.CashDao
import br.com.digio.androidtest.utilities.cash
import junit.framework.Assert.assertNotNull
import junit.framework.Assert.assertNull
import kotlinx.coroutines.runBlocking
import org.hamcrest.Matchers
import org.junit.runner.RunWith
import org.hamcrest.MatcherAssert.assertThat
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import androidx.test.ext.junit.runners.AndroidJUnit4


@RunWith(AndroidJUnit4::class)
class CashDaoTest {
    private lateinit var database: AppDatabase
    private lateinit var cashDao: CashDao

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun createDb() = runBlocking {
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        database = Room.inMemoryDatabaseBuilder(context, AppDatabase::class.java).build()
        cashDao = database.cashDao()
        cashDao.insert(cash)
    }

    @After
    fun closeDb() {
        database.close()
    }

    @Test
    fun get_whenContainCash_returnNotNull() = runBlocking {
        assertNotNull( cashDao.get())
    }

    @Test
    fun get_whenIsDefaultValue_returnCash(){
        assertThat(cashDao.get()?.id, Matchers.equalTo(cash.id))
    }

    @Test
    fun get_whenDeleteCash_returnNull(){
        cashDao.delete(cash)
        assertNull(cashDao.get())
    }


}