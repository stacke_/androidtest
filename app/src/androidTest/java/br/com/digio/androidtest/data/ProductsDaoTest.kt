package br.com.digio.androidtest.data

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.platform.app.InstrumentationRegistry
import br.com.digio.androidtest.data.local.AppDatabase
import br.com.digio.androidtest.data.local.ProductsDao
import br.com.digio.androidtest.utilities.products
import kotlinx.coroutines.runBlocking
import org.hamcrest.Matchers
import org.junit.runner.RunWith
import org.hamcrest.MatcherAssert.assertThat
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import androidx.test.ext.junit.runners.AndroidJUnit4

@RunWith(AndroidJUnit4::class)
class ProductsDaoTest {
    private lateinit var database: AppDatabase
    private lateinit var productsDao: ProductsDao


    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun createDb() = runBlocking {
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        database = Room.inMemoryDatabaseBuilder(context, AppDatabase::class.java).build()
        productsDao = database.productsDao()
        productsDao.insertAll(products.sortedBy { it.id })
    }

    @After
    fun closeDb() {
        database.close()
    }

    @Test
    fun getAll_whenListContainsProducts_returnSize() = runBlocking {
        val productsList = productsDao.getAll()
         assertThat(productsList?.size, Matchers.equalTo(3))
    }

    @Test
    fun getAll_whenListIsDefaultValue_returnListSortedById(){
        val productsList = productsDao.getAll()
        assertThat(productsList?.get(0)?.id, Matchers.equalTo(products[0].id))
        assertThat(productsList?.get(1)?.id, Matchers.equalTo(products[1].id))
        assertThat(productsList?.get(2)?.id, Matchers.equalTo(products[2].id))
    }
    @Test
    fun getAll_whenDeleteAllProducts_returnIsEmpty(){
        productsDao.deleteAll()
        productsDao.getAll()?.let{productsList->assert(productsList.isEmpty())}
    }


}