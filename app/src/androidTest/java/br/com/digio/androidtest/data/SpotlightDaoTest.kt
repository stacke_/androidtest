package br.com.digio.androidtest.data

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.platform.app.InstrumentationRegistry
import br.com.digio.androidtest.data.local.AppDatabase
import br.com.digio.androidtest.data.local.SpotlightDao
import br.com.digio.androidtest.utilities.spotlights
import kotlinx.coroutines.runBlocking
import org.hamcrest.Matchers
import org.hamcrest.MatcherAssert.assertThat
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import androidx.test.ext.junit.runners.AndroidJUnit4

@RunWith(AndroidJUnit4::class)
class SpotlightDaoTest {
    private lateinit var database: AppDatabase
    private lateinit var spotlightDao: SpotlightDao

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun createDb() = runBlocking {
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        database = Room.inMemoryDatabaseBuilder(context, AppDatabase::class.java).build()
        spotlightDao = database.spotlightDao()
        spotlightDao.insertAll(spotlights.sortedBy { it.id })
    }

    @After
    fun closeDb() {
        database.close()
    }

    @Test
    fun getAll_whenListContainsSpotlights_returnSize() = runBlocking {
        val spotlightList = spotlightDao.getAll()
         assertThat(spotlightList?.size, Matchers.equalTo(2))
    }

    @Test
    fun getAll_whenListIsDefaultValue_returnListSortedById(){
        val spotlightList = spotlightDao.getAll()
        assertThat(spotlightList?.get(0)?.id, Matchers.equalTo(spotlights[0].id))
        assertThat(spotlightList?.get(1)?.id, Matchers.equalTo(spotlights[1].id))
    }
    @Test
    fun getAll_whenDeleteAllSpotlight_returnIsEmpty(){
        spotlightDao.deleteAll()
        spotlightDao.getAll()?.let{spotlightList->assert(spotlightList.isEmpty())}
    }


}