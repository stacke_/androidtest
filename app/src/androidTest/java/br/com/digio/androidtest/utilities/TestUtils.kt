package br.com.digio.androidtest.utilities

import br.com.digio.androidtest.model.Cash
import br.com.digio.androidtest.model.Product
import br.com.digio.androidtest.model.Spotlight

/**
 * [Products] object used for tests
 * */

val products = listOf(
    Product(
        id = 1,
        imageURL = "https://www.gstatic.com/devrel-devsite/prod/v70c9aa38be5a41f2acdfd6deb7424dc7b523d8a488274535f707585ca8d2cdd3/android/images/lockup.svg",
        name = "Product 1 - Name",
        description = "Product 1 - Description"
    ),
    Product(
        id = 2,
        imageURL = "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse4.mm.bing.net%2Fth%3Fid%3DOIP.bB541NsW8WRXZJUIPXzrVgHaFj%26pid%3DApi&f=1",
        name = "Product 2 - Name",
        description = "Product 2 - Description"
    ),
    Product(
        id = 3,
        imageURL = "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse1.mm.bing.net%2Fth%3Fid%3DOIP.6JwJEMYSCOFqs-2RlTvHjAHaEo%26pid%3DApi&f=1",
        name = "Product 3 - Name",
        description = "Product 3 - Description"
    )
)
/**
 * [Spotlights] object used for tests
 * */
val spotlights = listOf(
    Spotlight(
        id = 1,
        bannerURL = "https://www.gstatic.com/devrel-devsite/prod/v70c9aa38be5a41f2acdfd6deb7424dc7b523d8a488274535f707585ca8d2cdd3/android/images/lockup.svg",
        name = "Spotlight 1 - Name",
        description = "Spotlight 1 - Description"
    ), Spotlight(
        id = 2,
        bannerURL = "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse4.mm.bing.net%2Fth%3Fid%3DOIP.bB541NsW8WRXZJUIPXzrVgHaFj%26pid%3DApi&f=1",
        name = "Spotlight 2 - Name",
        description = "Spotlight 2 - Description"
    )
)
/**
 * [Cash] object used for tests
 * */
val cash = Cash(
    id = 1,
    bannerURL = "https://www.gstatic.com/devrel-devsite/prod/v70c9aa38be5a41f2acdfd6deb7424dc7b523d8a488274535f707585ca8d2cdd3/android/images/lockup.svg",
    title = "Cash"
)


