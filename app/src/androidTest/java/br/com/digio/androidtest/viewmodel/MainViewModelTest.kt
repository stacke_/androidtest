package br.com.digio.androidtest.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.platform.app.InstrumentationRegistry
import br.com.digio.androidtest.MainCoroutineRule
import br.com.digio.androidtest.data.DigioProductsRepository
import br.com.digio.androidtest.data.local.AppDatabase
import br.com.digio.androidtest.runBlockingTest
import br.com.digio.androidtest.ui.MainViewModel
import dagger.hilt.android.testing.HiltAndroidTest
import dagger.hilt.android.testing.HiltAndroidRule
import org.junit.*
import org.junit.rules.RuleChain
import java.lang.AssertionError
import javax.inject.Inject

@HiltAndroidTest
class MainViewModelTest {

    private lateinit var appDatabase: AppDatabase
    private lateinit var viewModel: MainViewModel
    private var hiltRule = HiltAndroidRule(this)
    private val instantTaskExecutorRule = InstantTaskExecutorRule()
    private val coroutineRule = MainCoroutineRule()


    @get:Rule
    val rule = RuleChain
        .outerRule(hiltRule)
        .around(instantTaskExecutorRule)
        .around(coroutineRule)

    @Inject
    lateinit var digioProductsRepository: DigioProductsRepository

    @Before
    fun setUp() {
        hiltRule.inject()
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        appDatabase = Room.inMemoryDatabaseBuilder(context, AppDatabase::class.java).build()
        viewModel = MainViewModel(digioProductsRepository)
    }

    @After
    fun databaseDown() {
        appDatabase.close()
    }

    @Suppress("BlockingMethodInNonBlockingContext")
    @Test
    @Throws(InterruptedException::class)
    fun getProducts_whenDigioProductsIsNull_returnNullValue() = coroutineRule.runBlockingTest {
        Assert.assertNull(viewModel.digioProducts.value)
    }

}