package br.com.digio.androidtest

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class DigioApplication: Application()