package br.com.digio.androidtest.data

import br.com.digio.androidtest.data.local.CashDao
import br.com.digio.androidtest.data.local.ProductsDao
import br.com.digio.androidtest.data.local.SpotlightDao
import br.com.digio.androidtest.model.DigioProducts
import br.com.digio.androidtest.data.remote.DigioProductsDataSource
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject
import br.com.digio.androidtest.model.Result
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.flowOn

class DigioProductsRepository @Inject constructor(
    private val digioProductsDataSource: DigioProductsDataSource,
    private val cashDao: CashDao,
    private val productsDao: ProductsDao,
    private val spotlightDao: SpotlightDao
) {

    suspend fun fetchDigioProducts(): Flow<Result<DigioProducts>?>{
        return flow {
            emit(fetchDigioProductsCached())
            emit(Result.loading())
            val result = digioProductsDataSource.fetchDigioProducts()
            if(result.status == Result.Status.SUCCESS){
                deleteDigioProducts(result.data)
                insertDigioProducts(result.data)
            }
            emit(result)
        }.flowOn(Dispatchers.IO)
    }

    private fun deleteDigioProducts(digioProducts: DigioProducts?){
        digioProducts?.let{ digioProducts->
            digioProducts.cash?.apply { cashDao.delete(this) }
            digioProducts.products?.apply { productsDao.deleteAll() }
            digioProducts.spotlight?.apply { spotlightDao.deleteAll() }
        }
    }
    private fun insertDigioProducts(digioProducts: DigioProducts?){
        digioProducts?.let{ digioProducts->
            digioProducts.cash?.apply { cashDao.insert(this) }
            digioProducts.products?.apply { productsDao.insertAll(this) }
            digioProducts.spotlight?.apply { spotlightDao.insertAll(this) }
        }
    }

    private fun fetchDigioProductsCached(): Result<DigioProducts>{
        val cash = cashDao.get()
        val produts = productsDao.getAll()
        val spotlights = spotlightDao.getAll()
        return Result.success(DigioProducts(cash,produts,spotlights))
    }

}