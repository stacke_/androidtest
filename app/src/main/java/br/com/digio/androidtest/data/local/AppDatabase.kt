package br.com.digio.androidtest.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import br.com.digio.androidtest.data.Converters
import br.com.digio.androidtest.model.Cash
import br.com.digio.androidtest.model.Product
import br.com.digio.androidtest.model.Spotlight


@Database(entities = [Cash::class,Product::class,Spotlight::class], version = 1)
@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun cashDao(): CashDao
    abstract fun productsDao(): ProductsDao
    abstract fun spotlightDao(): SpotlightDao
}