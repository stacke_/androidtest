package br.com.digio.androidtest.data.local

import androidx.room.*
import br.com.digio.androidtest.model.Cash

@Dao
interface CashDao {

    @Query("SELECT * FROM cash")
    fun get(): Cash?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(cash: Cash)

    @Delete
    fun delete(cash: Cash)


}