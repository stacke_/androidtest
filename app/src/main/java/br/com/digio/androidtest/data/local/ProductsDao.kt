package br.com.digio.androidtest.data.local

import androidx.room.*
import br.com.digio.androidtest.model.Product

@Dao
interface ProductsDao {

    @Query("SELECT * FROM product")
    fun getAll(): List<Product>?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(products: List<Product>)

    @Query("DELETE FROM product")
    fun deleteAll()

}