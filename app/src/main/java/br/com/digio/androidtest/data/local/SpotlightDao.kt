package br.com.digio.androidtest.data.local

import androidx.room.*
import br.com.digio.androidtest.model.Spotlight

@Dao
interface SpotlightDao {

    @Query("SELECT * FROM spotlight")
    fun getAll(): List<Spotlight>?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(spotlights: List<Spotlight>)

    @Query("DELETE FROM spotlight")
    fun deleteAll()
}