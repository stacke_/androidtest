package br.com.digio.androidtest.data.remote

import br.com.digio.androidtest.model.DigioProducts
import br.com.digio.androidtest.network.DigioEndpoint
import retrofit2.Response
import retrofit2.Retrofit
import javax.inject.Inject
import br.com.digio.androidtest.model.Result
import br.com.digio.androidtest.utils.ErrorUtils


class DigioProductsDataSource @Inject constructor(private val retrofit: Retrofit){

    suspend fun fetchDigioProducts(): Result<DigioProducts>{
        val service = retrofit.create(DigioEndpoint::class.java)
        return getResponse(
            request = {service.getProducts()},
            defaultErrorMessage = "Error fetching products"
        )
    }

    private suspend fun <T> getResponse(request: suspend () -> Response<T>, defaultErrorMessage: String): Result<T> {
        return try {
            val result = request.invoke()
            if (result.isSuccessful) {
                return Result.success(result.body())
            } else {
                val errorResponse = ErrorUtils.parseError(result, retrofit)
                Result.error(errorResponse?.status_message ?: defaultErrorMessage, errorResponse)
            }
        } catch (e: Throwable) {
            Result.error("Unknown Error", null)
        }
    }

}