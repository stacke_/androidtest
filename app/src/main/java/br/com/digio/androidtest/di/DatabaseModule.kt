package br.com.digio.androidtest.di

import android.content.Context
import androidx.room.Room
import br.com.digio.androidtest.data.local.AppDatabase
import br.com.digio.androidtest.data.local.CashDao
import br.com.digio.androidtest.data.local.ProductsDao
import br.com.digio.androidtest.data.local.SpotlightDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object DatabaseModule {

    @Provides
    @Singleton
    fun provideAppDatabase(@ApplicationContext appContext: Context): AppDatabase {
        return Room.databaseBuilder(
            appContext,
            AppDatabase::class.java,
            "app.db"
        ).build()
    }

    @Provides
    fun provideCashDao(appDatabase: AppDatabase): CashDao {
        return appDatabase.cashDao()
    }

    @Provides
    fun provideProductsDao(appDatabase: AppDatabase): ProductsDao {
        return appDatabase.productsDao()
    }

    @Provides
    fun provideSpotlightDao(appDatabase: AppDatabase): SpotlightDao {
        return appDatabase.spotlightDao()
    }
}