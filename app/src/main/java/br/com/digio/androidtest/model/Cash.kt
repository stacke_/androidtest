package br.com.digio.androidtest.model

import androidx.annotation.NonNull
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Cash(
    @NonNull
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    val bannerURL: String,
    val title: String
)