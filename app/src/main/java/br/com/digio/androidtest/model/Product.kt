package br.com.digio.androidtest.model

import androidx.annotation.NonNull
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Product(
    @NonNull
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    val imageURL: String,
    val name: String,
    val description: String
)