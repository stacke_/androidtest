package br.com.digio.androidtest.network

import br.com.digio.androidtest.model.DigioProducts
import retrofit2.Response
import retrofit2.http.GET

interface DigioEndpoint {
    @GET("sandbox/products")
    suspend fun getProducts(): Response<DigioProducts>
}