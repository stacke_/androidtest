package br.com.digio.androidtest.ui


import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.text.SpannableString
import android.text.Spanned
import android.text.style.ForegroundColorSpan
import android.view.View
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import br.com.digio.androidtest.ui.adapter.ProductAdapter
import br.com.digio.androidtest.R
import br.com.digio.androidtest.databinding.ActivityMainBinding
import br.com.digio.androidtest.ui.adapter.SpotlightAdapter
import br.com.digio.androidtest.model.Result

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private val productAdapter: ProductAdapter by lazy {
        ProductAdapter()
    }
    private val spotlightAdapter: SpotlightAdapter by lazy {
        SpotlightAdapter()
    }

    private lateinit var viewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding : ActivityMainBinding  = DataBindingUtil.setContentView(this,R.layout.activity_main)
        viewModel = ViewModelProvider(this)[MainViewModel::class.java]
        binding.mainViewModel = viewModel
        onSetupView(binding)
        setObserver(binding)
        setupDigioCashText(binding)
    }

    private fun onSetupView(binding: ActivityMainBinding){
        binding.apply {
            recyMainProducts.layoutManager = LinearLayoutManager(this@MainActivity, LinearLayoutManager.HORIZONTAL, false)
            recyMainProducts.adapter = productAdapter
            recyMainSpotlight.layoutManager = LinearLayoutManager(this@MainActivity, LinearLayoutManager.HORIZONTAL, false)
            recyMainSpotlight.adapter = spotlightAdapter
            body.visibility = View.GONE
            loadDigioContainer.visibility = View.VISIBLE
        }
    }

    private fun setObserver(binding: ActivityMainBinding){
        binding.mainViewModel?.digioProducts?.observe(
            this,
            Observer {result->
                when(result.status){
                    Result.Status.SUCCESS ->{
                        binding.loadDigioContainer.visibility = View.GONE
                        binding.body.visibility = View.VISIBLE
                        productAdapter.products = result.data?.products?: emptyList()
                        spotlightAdapter.spotlights = result.data?.spotlight?: emptyList()
                    }
                    Result.Status.ERROR->{
                        val message = getString(R.string.error)
                        binding.loadDigioContainer.visibility = View.GONE
                        Toast.makeText(this@MainActivity, message, Toast.LENGTH_SHORT).show()
                    }
                    Result.Status.LOADING ->{
                        binding.loadDigioContainer.visibility = View.VISIBLE
                    }

                }
            }
        )
    }

    private fun setupDigioCashText(binding: ActivityMainBinding) {
        val digioCacheText = "digio Cache"
        binding.txtMainDigioCash.text = SpannableString(digioCacheText).apply {
            setSpan(
                ForegroundColorSpan(
                    ContextCompat.getColor(this@MainActivity, R.color.blue_darker)
                ),
                0,
                5,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
            )
            setSpan(
                ForegroundColorSpan(
                    ContextCompat.getColor(this@MainActivity, R.color.font_color_digio_cash)
                ),
                6,
                digioCacheText.length,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
            )
        }
    }
}