package br.com.digio.androidtest.ui


import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import br.com.digio.androidtest.data.DigioProductsRepository
import br.com.digio.androidtest.model.DigioProducts
import br.com.digio.androidtest.model.Result
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(private val digioProductsRepository: DigioProductsRepository) : ViewModel() {

    private val _digioProducts = MutableLiveData<Result<DigioProducts>>()
    val digioProducts = _digioProducts

    init {
        fetchDigioProducts()
    }

    private fun fetchDigioProducts(){
        viewModelScope.launch {
            digioProductsRepository.fetchDigioProducts().collect {
                _digioProducts.value = it
            }
        }
    }
}