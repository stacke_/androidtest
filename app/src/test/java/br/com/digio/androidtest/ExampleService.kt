package br.com.digio.androidtest

import br.com.digio.androidtest.model.DigioProducts
import br.com.digio.androidtest.network.DigioEndpoint
import com.google.gson.Gson
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.runBlocking

class ExampleService(
    private val service: DigioEndpoint
) {

    fun mockDigioProducts(): DigioProducts =
        Gson().fromJson(
            """
                {
                  "spotlight": [
                    {
                      "name": "Recarga",
                      "bannerURL": "https://s3-sa-east-1.amazonaws.com/digio-exame/recharge_banner.png",
                      "description": "Agora ficou mais fácil colocar créditos no seu celular! A digio Store traz a facilidade de fazer recargas... direto pelo seu aplicativo, com toda segurança e praticidade que você procura."
                    }
                  ],
                  "products": [
                    {
                      "name": "XBOX",
                      "imageURL": "https://s3-sa-east-1.amazonaws.com/digio-exame/xbox_icon.png",
                      "description": "Com o e-Gift Card Xbox você adquire créditos para comprar games, música, filmes, programas de TV e muito mais!"
                    }
                  ],
                  "cash": {
                    "title": "digio Cash",
                    "bannerURL": "https://s3-sa-east-1.amazonaws.com/digio-exame/cash_banner.png",
                    "description": "Dinheiro na conta sem complicação. Transfira parte do limite do seu cartão para sua conta."
                  }
                }
            """.trimIndent(),
            DigioProducts::class.java
        )

    suspend fun example(): Flow<DigioProducts> {
       return flow {
           val products = service.getProducts()
           emit(products.body() ?: mockDigioProducts())
       }
    }
}