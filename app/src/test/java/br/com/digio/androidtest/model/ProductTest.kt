package br.com.digio.androidtest.model

import org.junit.Before
import org.junit.Test
import junit.framework.TestCase.assertEquals
import junit.framework.TestCase.assertNotNull

class ProductTest {
    private lateinit var productDefaultValue: Product

    @Before fun setUp(){
        productDefaultValue = Product(
            id = 0,
            imageURL = "https://www.gstatic.com/devrel-devsite/prod/v70c9aa38be5a41f2acdfd6deb7424dc7b523d8a488274535f707585ca8d2cdd3/android/images/lockup.svg",
            name = "Product 0 - Name",
            description = "Product 0 - Description"
            )
    }

    @Test fun product_whenValueIsDefault_returnsDefaultValue(){
        assertEquals(0,productDefaultValue.id)
        assertEquals("https://www.gstatic.com/devrel-devsite/prod/v70c9aa38be5a41f2acdfd6deb7424dc7b523d8a488274535f707585ca8d2cdd3/android/images/lockup.svg",productDefaultValue.imageURL)
        assertEquals("Product 0 - Name",productDefaultValue.name)
        assertEquals("Product 0 - Description", productDefaultValue.description)
    }

    @Test fun product_whenProductIdIsNotNull_returnsDefaultValue(){
       assertNotNull(productDefaultValue)
    }

}