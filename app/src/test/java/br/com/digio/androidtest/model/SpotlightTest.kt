package br.com.digio.androidtest.model

import junit.framework.TestCase
import org.junit.Before
import org.junit.Test

class SpotlightTest {
    private lateinit var spotlightDefaultValue: Spotlight

    @Before
    fun setUp(){
        spotlightDefaultValue = Spotlight(
            id = 0,
            bannerURL = "https://www.gstatic.com/devrel-devsite/prod/v70c9aa38be5a41f2acdfd6deb7424dc7b523d8a488274535f707585ca8d2cdd3/android/images/lockup.svg",
            name = "Spotlight 0 - Name",
            description = "Spotlight 0 - Description"
        )
    }

    @Test
    fun spotlight_whenValueIsDefault_returnsDefaultValue(){
        TestCase.assertEquals(0, spotlightDefaultValue.id)
        TestCase.assertEquals(
            "https://www.gstatic.com/devrel-devsite/prod/v70c9aa38be5a41f2acdfd6deb7424dc7b523d8a488274535f707585ca8d2cdd3/android/images/lockup.svg",
            spotlightDefaultValue.bannerURL
        )
        TestCase.assertEquals("Spotlight 0 - Name", spotlightDefaultValue.name)
        TestCase.assertEquals("Spotlight 0 - Description", spotlightDefaultValue.description)
    }

    @Test
    fun product_whenProductIdIsNotNull_returnsDefaultValue(){
        TestCase.assertNotNull(spotlightDefaultValue)
    }
}